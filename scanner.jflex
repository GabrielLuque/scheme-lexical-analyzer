import java.util.*;
%%

%class scheme_search
%standalone
%line
%column

//Definitions
// We aren't interested in grammar at the moment.

program = {form}*

form = {definition} | {expresion}

definition  = {variable_definition}
			| {syntax_definition}
			| \(begin {definition}*\)
			| \(let-syntax \({syntax_binding}*\) {definition}*\)
			| \(letrec-syntax \({syntax_binding}*\) {definition}*\)
			| {derived_definition}
			
variable_definition = \(define {variable} {expression}\)
					| \(define \({variable} {variable}*\) {body}\)
					| \(define \({variable} {variable}* . {variable}\) {body}\)
					


body              = {definition}* {expression}+
syntax_definition = \(define-syntax {keyword} {transformer_expression}\)

syntax_binding    = \({keyword} {transformer_expression}\)

variable          = {identifier}
// With jflex alone this definition of keyword doesn't seem to be appropriate as it'll conflict with variable. 
//keyword           = {identifier}



expresion   = {constant}
		    | {variable}
			| \({operator} {expresion}\)
		    | \(quote {datum} | \' {datum}
		    | \(lambda {formals} {body}\)
		    | \(if {expresion} {expresion} {expresion}\) | \(if {expresion} {expresion}\)
		    | \(set! {variable} {expresion}\)
		    | {application}
		    | (let-syntax \({syntax_binding}*\) {expression}+)
		    | (letrec-syntax \({syntax_binding}*\) {expression}+)
		    | {derived_expression}
			| \({keyword} {expresion}\)

operator    = ["*"|"/"|"+"|"-"|"="|"<"|">"]
constant    = {boolean}|{number}|{character}|{string}
formals     = {variable}|\({variable}*\)|\({variable}+\.{variable}\)
application = \({expression} {expression}*\)



// Misc.
comment        =  ;.*
whitespace     =[ \n\r\t]+
empty          = " "
paranthesis    = [(|)|[|]]
rev_identifier = define|let|begin|if|quote|lambda|atom|define-syntax|letrec|cons|loop

// Identifiers
letter     = [a-zA-Z]+
digit      = [0-9]+
identifier = {initial}{subsequent}*|\+|\-|\.\.\.
initial    = {letter}|\!|\$|\%|\&|\*|\/|\:|\<|\=|\>|\?|\~|\_|\^ 
subsequent = {initial}|{digit}|\.|\+|-

// Data
datum = {boolean}|{number}|{character}|{string}

boolean         = #t|#f
number          = {num10}|{num8}|{num16}|{num2} 
character       = #\\.|#\\newline|#\\space
string          = \"{stringCharacter}*\"
stringCharacter = \\\"|\\\\|[^\\\\\\\"]
symbol          = {identifier}
list            = {abbreviation}
abbreviation    = \'{datum}|\`{datum}|,{datum}|,@{datum}
vector          = #{datum}*

//Numbers

 //Number
  num10 = {prefix10}?{complex10}
  num8  = {prefix8}{complex8}
  num16 = {prefix16}{complex16}
  num2  = {prefix2}{complex2}
  
 //Complex
  complex10 = {real10}|{real10}@{real10}
			| {real10} \+ {imag10} | {real10} - {imag10}
			| \+ {imag10} | - {imag10}
  complex8  = {real8}|{real8}@{real8}
			| {real8} \+ {imag8} | {real8} - {imag8}
			| \+ {imag8} | - {imag8} 
  complex16 = {real16}|{real16}@{real16}
			| {real16} \+ {imag16} | {real16} - {imag16}
			| \+ {imag16} | - {imag16}
  complex2  = {real2}|{real2}@{real2}
			| {real2} \+ {imag2} | {real2} - {imag2}
			| \+ {imag2} | - {imag2}
  
 //Imaginary
  imag10 = i|{ureal10}i
  imag8  = i|{ureal8} i 
  imag16 = i|{ureal16}i
  imag2  = i|{ureal2} i

 //real
  real10 = {sign}?{ureal10}
  real8  = {sign}?{ureal8}
  real16 = {sign}?{ureal16}
  real2  = {sign}?{ureal2}

 //ureal
  ureal10 = {uinteger10}|{uinteger10} \/ {uinteger10}|{decimal10}
  ureal8  = {uinteger8}|{uinteger8} \/ {uinteger8}|{decimal8} 
  ureal16 = {uinteger16}|{uinteger16} \/ {uinteger16}|{decimal16}
  ureal2  = {uinteger2}|{uinteger2} \/ {uinteger2}|{decimal2}
  
 //uinteger
  uinteger10 = {digit10}+#*|{digit10}
  uinteger8  = {digit8}+#*|{digit8}
  uinteger16 = {digit16}+#*|{digit16}
  uinteger2  = {digit2}+#*|{digit2}
  
 //prefix
  prefix10 = {radix10}{exactness}? |{exactness}?{radix10}
  prefix8  = {radix8}{exactness}? | {exactness}?{radix8} 
  prefix16 = {radix16}{exactness}? |{exactness}?{radix16}
  prefix2  = {radix2}{exactness}? | {exactness}?{radix2}
  

 //decimals
 decimal10 = {uinteger10}{exp}
			|\.{digit10}+#*{suffix}?
			|{digit10}+\.{digit10}*#*{suffix}?
			|{digit10}+ #+ \. #*{suffix}?
 decimal8 = {uinteger8}{exp}
			|\.{digit8}+#*{suffix}?
			|{digit8}+\.{digit8}*#*{suffix}?
			|{digit8}+ #+ \. #*{suffix}?
 decimal16 = {uinteger16}{exp}
			|\.{digit16}+#*{suffix}?
			|{digit16}+\.{digit16}*#*{suffix}?
			|{digit16}+ #+ \. #*{suffix}?
 decimal2 = {uinteger2}{exp}
			|\.{digit2}+#*{suffix}?
			|{digit2}+\.{digit2}*#*{suffix}?
			|{digit2}+ #+ \. #*{suffix}?

 suffix    = {exp}
 exp       = {expMarker}{sign}?{digit10}+
 expMarker = e|s|f|d|l
 sign      = \+|-
 exactness = #i|#e
 
 
 //Radix
 radix10 = #d
 radix8  = #o
 radix16 = #x
 radix2  = #b
 

 
 //Digits
  digit   = [0-9]
  digit10 = {digit}
  digit8  = [0-7]
  digit16 =  {digit} | [a-f]
  digit2  = 0|1
  
// This will add the pieces of scheme code into a token value lists
// This list is divided in literal, comments, operator, keywords, and identifiers

%{
List<String> literals = new ArrayList<String>();
List<String> comments = new ArrayList<String>();
List<String> operators= new ArrayList<String>();
List<String> keywords = new ArrayList<String>();
List<String> identifiers = new ArrayList<String>();
%}



%eof{
System.out.println("\n Identifiers: ");
for(int i=0;i<identifiers.size();i++){
	System.out.println("\t"+"\""+identifiers.get(i)+"\"");
}	
System.out.println("\n Keywords: ");
for(int i=0;i<keywords.size();i++){
	System.out.println("\t"+"\""+keywords.get(i)+"\"");
}	
System.out.println("\n Literals: ");
for(int i=0;i<literals.size();i++){
	System.out.println("\t"+"\""+literals.get(i)+"\"");
}

System.out.println("\n Comments: ");
for(int i=0;i<comments.size();i++){
	System.out.println("\t"+"\""+comments.get(i)+"\"");
}

System.out.println("\n Operators: ");
for(int i=0;i<operators.size();i++){
	System.out.println("\t"+"\""+operators.get(i)+"\"");
}

%eof}

%%       


{comment}     {comments.add("Comment: "+yytext());}
{operator}    {operators.add("Operator: "+yytext());}


{rev_identifier} {keywords.add("Reserved phrases: "+yytext());}



{paranthesis} {}


{character} {literals.add("Character: "+yytext());}
{string}    {literals.add("String: "+yytext());}

{num10}   {literals.add("Number base 10: "+yytext());}	
{num8}    {literals.add("Number base 8: "+yytext());}	
{num16}   {literals.add("Number base 16: "+yytext());}	
{num2}    {literals.add("Number base 2: "+yytext());}	

{datum}   {literals.add("Datum: "+yytext());}



{identifier} {identifiers.add("Identifier: "+yytext());}    
{variable}   {identifiers.add("Variable: "+yytext());}

	
